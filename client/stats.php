<!doctype html>
<html>

<head>
    <title>Stats</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="favicon.png">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/open-iconic-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/datatables.min.css" rel="stylesheet" />
</head>

<?php

$name="[TOUS]";
$debut="";
$fin="";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = (empty($_POST['fname'])) ? '[TOUS]' : $_POST['fname'];
  $debut = (empty($_POST['fdebut'])) ? '' : $_POST['fdebut'];
  $fin = (empty($_POST['fdebut'])) ? '' : $_POST['ffin'];
}

$db = new SQLite3('stats.sqlite3', SQLITE3_OPEN_READONLY);

$res = $db->query('SELECT DISTINCT glouton FROM stats ORDER BY glouton');
$allgloutons = array();
while ($row = $res->fetchArray(SQLITE3_NUM)) {
    $allgloutons[] = $row[0];
}

$sel_name = "<select name='fname' class='custom-select ml-1'>\n";
$sel = ($name=="[TOUS]")?'selected':'';
$sel_name .= "<option value='[TOUS]' {$sel}>[TOUS]</option>\n";
foreach ($allgloutons as $value) {
	if ($value!="[TOUS]"){
		$sel = ($name==$value)?'selected':'';
		$sel_name .= "<option value=\"".$value."\""." {$sel}>".$value."</option>\n";
	}
}
$sel_name .= "</select>\n";


$res = $db->query('SELECT MIN(datef) as \'min\', MAX(datef) as \'max\' FROM stats');
$row = $res->fetchArray(SQLITE3_ASSOC);
$date_min = $row["min"];
$date_max = $row["max"];
?>

<body>
    <div class="content">
        <nav class="navbar sticky-top navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <img src="favicon.png" width="50" height="30" class="d-inline-block align-top" alt="">
                <span class="h3 ml-2">Fournil</span>
            </a>
            <span class="navbar-text">
                <span id="send-json-icon" class="oi oi-graph" aria-hidden="true"></span>
                <span id="send-json-label" class="ml-1">Stats</span>
            </span>
        </nav>
        <div class="w-75 m-auto pt-3">
			<div class="content">
				<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" class="form-inline">
					<label for="fname">Nom : </label>
					<?php echo $sel_name;?>
					<label for="fdebut" class="ml-4" title="entre <?php echo $date_min;?> et <?php echo $date_max;?>">P&eacute;riode de : </label>
					<input type="date" class="form-control ml-1" id="fdebut" name="fdebut" value="<?php echo $debut;?>" min="<?php echo $date_min;?>" max="<?php echo $date_max;?>">
					<label for="ffin" class="ml-1"> &agrave; : </label>
					<input type="date" class="form-control ml-1" id="ffin" name="ffin" value="<?php echo $fin;?>" min="<?php echo $date_min;?>" max="<?php echo $date_max;?>">
					<button type="submit" class="btn btn-secondary ml-4">Envoyer</button>
				</form>
			</div>
		<hr/>
<?php

$alias = [
			"p500" => "Froment 500g",
			"p1000" => "Froment 1kg",
			"p1500" => "Froment 1,5kg",
			"p2000" => "Froment 2kg",
			"g650" => "Engrain 650g",
			"s500" => "Seigle 500g",
			"e640" => "Épices 640",
			"e900" => "Épices 900",
			"b75" => "Biscuits 75",
			"b150" => "Biscuits 150",
			"verse" => "Somme vers&eacute;e"
		];

$sum = "";
foreach ($alias as $p => $a) {
	if ($p == "g650") {
		//~ Ajout du total de poids en froment
		$sum .= ", SUM(\"p500\"*0.5+\"p1000\"+\"p1500\"*1.5+\"p2000\"*2) as 'Froment<br>Total kg'";
	}
	$sum .= ", SUM(\"{$p}\") as '{$a}'";
}

$qp = ($debut == "") ? "" : " AND datef >= '{$debut}' AND datef <= '{$fin}'";

$q = "SELECT count(datef) as 'Nombre de fournées'".$sum." FROM stats WHERE glouton=\"".$name."\"".$qp;
//~ echo $q;
$statement = $db->prepare($q);
$res = $statement->execute();

$nb_cols = $res->numColumns();

$tab = array();
while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
    $tab[] = $row;
}
$glouton = ($name == "[TOUS]") ? 'tout le monde' : "\"".$name."\"";
$periode = ($debut == "") ? 'de toutes les fourn&eacute;es': "des fourn&eacute;es du {$debut} au {$fin}";

echo "<h3>Total {$periode} pour {$glouton}</h3>\n";
if ($tab[0]["Nombre de fournées"] == 0){
	echo "<h3>RIEN</h3>\n";
}else{
	echo "<table id='stats' class='table text-center align-middle table-striped table-bordered'>\n";
	echo "<thead>\n";
	echo "<tr>\n";
	for ($i = 0; $i < $nb_cols; $i++) {
		echo "<th>{$res->columnName($i)}</th>\n";
	}
	echo "</tr>\n";
	echo "</thead>\n";
	echo "<tbody>\n";
	foreach ($tab as $row) {
		echo "<tr>\n";
		for ($i = 0; $i < $nb_cols; $i++) {
			echo "<td>{$row[$res->columnName($i)]}</td>\n";
		}
		echo "</tr>\n";
	}
	echo "</tbody>\n";
	echo "</table>\n";
	echo "<hr />\n";
	echo "<h3>D&eacute;tail<button id='export' class='btn btn-secondary ml-4' title='Exporter'><span class='oi oi-spreadsheet'></span></button></h3>\n";

	$sum = "";
	foreach ($alias as $p => $a) {
		$sum .= ", {$p} as '{$a}'";
	}
	$q = "SELECT datef as 'Fournée'".$sum." FROM stats WHERE glouton=\"".$name."\"".$qp;
//~ echo $q;
	$statement = $db->prepare($q);
	$res = $statement->execute();

	$nb_cols = $res->numColumns();

	$tab = array();
	while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
		$tab[] = $row;
	}

	echo "<table id='detail' class='table text-center align-middle table-sm table-striped table-bordered'>\n";
	echo "<thead>\n";
	echo "<tr>\n";
	for ($i = 0; $i < $nb_cols; $i++) {
		echo "<th>{$res->columnName($i)}</th>\n";
	}
	echo "</tr>\n";
	echo "</thead>\n";
	echo "<tbody>\n";
	foreach ($tab as $row) {
		echo "<tr>\n";
		for ($i = 0; $i < $nb_cols; $i++) {
			echo "<td>{$row[$res->columnName($i)]}</td>\n";
		}
		echo "</tr>\n";
	}
	echo "</tbody>\n";
	echo "</table>\n";
}

$per =($debut == '') ? 'Stats globales': "{$debut}_{$fin}";
$titre_csv = "{$name}_{$per}";

?>
</div>
</div>
<script>
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;

    csvFile = new Blob([csv], {type: "text/csv"});

    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function export_table_to_csv(html, filename) {
	var csv = [];
	var rows = html.querySelectorAll("table tr");
	
    for (var i = 0; i < rows.length; i++) {
		var row = [], cols = rows[i].querySelectorAll("td, th");
		
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
		csv.push(row.join(";"));
	}
    download_csv(csv.join("\n"), filename);
}

document.getElementById("export").addEventListener("click", function () {
    var html = document.getElementById("detail");
	export_table_to_csv(html, "<?php echo $titre_csv;?>.csv");
});
</script>
</body>
</html>
