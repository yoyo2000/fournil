<!doctype html>
<html>

<head>
    <title>Fournil Historique</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="favicon.png">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/open-iconic-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/datatables.min.css" rel="stylesheet" />
</head>

<body>
    <div class="content">
        <nav class="navbar sticky-top navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <img src="favicon.png" width="50" height="30" class="d-inline-block align-top" alt="">
                <span class="h3 ml-2">Fournil</span>
            </a>
            <span class="navbar-text">
                <span id="send-json-icon" class="oi oi-header" aria-hidden="true"></span>
                <span id="send-json-label" class="ml-1">Historique</span>
            </span>
        </nav>
        <div class="w-75 m-auto pt-3">
            <table id="hist" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Panier total</th>
                        <th>Froment</th>
                        <th>Seigle</th>
                        <th>Engrain</th>
                        <th>Épices</th>
                        <th>Biscuits</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?

                    try {
                        $json_output = file_get_contents("hist.json");
                        $json = json_decode($json_output);

                        /*Defining table Column headers depending upon JSON records*/
                        $temp .= "";

                        foreach ($json as $date => $fournee) {
                            $temp .= "<tr>";
                            $temp .= "<td>" . $date . "</td>";
                            $temp .= "<td>" . $fournee->panier . " €</td>";
                            if (!is_object($fournee->poids)) {
                                $temp .= "<td>" . $fournee->poids . " kg</td>";
                                $temp .= "<td>-</td>";
                                $temp .= "<td>-</td>";
                                $temp .= "<td>-</td>";
                                $temp .= "<td>-</td>";
                            } else {
                                $temp .= "<td>" . $fournee->poids->p . " kg</td>";
                                $temp .= "<td>" . $fournee->poids->s . " kg</td>";
                                $temp .= "<td>" . $fournee->poids->g . " kg</td>";
                                $temp .= "<td>" . $fournee->poids->e . " kg</td>";
                                $temp .= "<td>" . $fournee->poids->b . " kg</td>";
                            }
                            $temp .= "<td>";
                            $temp .= "<div class='btn-group' role='group'>";
                            $temp .= "<a target='_blank' href='hist_details.php?date=" . $date . "' class='btn btn-info px-3 btn-sm' title='Voir les détails'><span id='send-json-icon' class='oi oi-eye' aria-hidden='true'></span></a>";
                            $temp .= "<a href='hist_f/" . $date . ".json' class='btn btn-warning px-3 btn-sm' title='Télécharger' download><span id='down-json-icon' class='oi oi-cloud-download' aria-hidden='true'></span></a>";
                            $temp .= "</div>";
                            $temp .= "</td>";
                            $temp .= "</tr>";
                        }
                        echo $temp;
                    } catch (Exception $e) {
                        echo 'Erreur: ' . $e->getMessage() . "\n";
                    }

                    ?>
                </tbody>
            </table>
        </div>

    </div>
</body>
<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/datatables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#hist').DataTable({
            "order": [
                [0, "desc"]
            ],
            "language": {
                "sProcessing": "Traitement en cours...",
                "sSearch": "Rechercher&nbsp;:",
                "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty": "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix": "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst": "Premier",
                    "sPrevious": "Pr&eacute;c&eacute;dent",
                    "sNext": "Suivant",
                    "sLast": "Dernier"
                },
                "oAria": {
                    "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                },
                "select": {
                    "rows": {
                        _: "%d lignes séléctionnées",
                        0: "Aucune ligne séléctionnée",
                        1: "1 ligne séléctionnée"
                    }
                }
            },
            "columnDefs": [{
                "orderable": false,
                "targets": [7]
            }]
        });
    });
</script>

</html>