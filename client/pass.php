<?php
# Page Password Protect 2.13 http://www.zubrag.com/scripts/

$LOGIN_INFORMATION = array(
  'kir' => 'royal',
  'vodka' => 'orange'
);

// request login? true - show login and password boxes, false - password box only
define('USE_USERNAME', true);

// User will be redirected to this page after logout
define('LOGOUT_URL', 'index.php');

// time out after NN minutes of inactivity. Set to 0 to not timeout
define('TIMEOUT_MINUTES', 0);

// This parameter is only useful when TIMEOUT_MINUTES is not zero
// true - timeout time from last activity, false - timeout time from login
define('TIMEOUT_CHECK_ACTIVITY', true);

// timeout in seconds
$timeout = (TIMEOUT_MINUTES == 0 ? 0 : time() + TIMEOUT_MINUTES * 60);

// logout?
if (isset($_GET['logout'])) {
  setcookie("verify", '', $timeout, '/'); // clear password;
  header('Location: ' . LOGOUT_URL);
  exit();
}

if (!function_exists('showLoginPasswordProtect')) {
  // show login form
  function showLoginPasswordProtect($error_msg)
  {
    ?>
    <html>

    <head>
      <title>Halte là !</title>
      <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
      <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
      <link href="css/fonts.css" rel="stylesheet">
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
    </head>

    <body class="bg-dark">
      <div class="card border-warning text-white bg-dark mt-3" style="width:500px; margin-left:auto; margin-right:auto; text-align:center">
        <img class="card-img-top p-1" src="favicon.png" alt="Fournil">
        <div class="card-body pb-0">
          <form method="post">
            <div class="form-group">
              <input autofocus type="text" class="form-control" id="access_login" name="access_login" placeholder="Nom">
            </div>
            <div class="form-group mb-0">
              <input type="password" class="form-control" id="access_password" name="access_password" placeholder="Mot de passe">
              <?php
                  if ($error_msg != "") {
                    echo '<p class="text-danger mt-3 mb-0"><small>' . $error_msg . '</small></p>';
                  }
                  ?>
            </div>
            <button type="submit" class="btn btn-warning btn-lg text-dark mt-3">Entrer</button>
          </form>
        </div>
      </div>
      </div>
    </body>

    </html>

<?php
    die();
  }
}

// user provided password
if (isset($_POST['access_password'])) {

  $login = isset($_POST['access_login']) ? $_POST['access_login'] : '';
  $pass = $_POST['access_password'];
  if (
    !USE_USERNAME && !in_array($pass, $LOGIN_INFORMATION)
    || (USE_USERNAME && (!array_key_exists($login, $LOGIN_INFORMATION) || $LOGIN_INFORMATION[$login] != $pass))
  ) {
    showLoginPasswordProtect("Nom ou mot de passe incorrect");
  } else {
    // set cookie if password was validated
    setcookie("verify", md5($login . '%' . $pass), $timeout, '/');

    // Some programs (like Form1 Bilder) check $_POST array to see if parameters passed
    // So need to clear password protector variables
    unset($_POST['access_login']);
    unset($_POST['access_password']);
    unset($_POST['Submit']);
  }
} else {

  // check if password cookie is set
  if (!isset($_COOKIE['verify'])) {
    showLoginPasswordProtect("");
  }

  // check if cookie is good
  $found = false;
  foreach ($LOGIN_INFORMATION as $key => $val) {
    $lp = (USE_USERNAME ? $key : '') . '%' . $val;
    if ($_COOKIE['verify'] == md5($lp)) {
      $found = true;
      // prolong timeout
      if (TIMEOUT_CHECK_ACTIVITY) {
        setcookie("verify", md5($lp), $timeout, '/');
      }
      break;
    }
  }
  if (!$found) {
    showLoginPasswordProtect("");
  }
}
?>