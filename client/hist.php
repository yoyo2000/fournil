<?

try {
    $dossier = "hist_f";

    $new = json_decode($_POST["myData"], true);
    $entree = array_keys($new["historique"])[0];
    
    if(!is_dir($dossier)){
        mkdir($dossier);
    }
    file_put_contents($dossier."/".$entree.".json",json_encode($new["historique"][$entree], JSON_UNESCAPED_UNICODE));

    $new_hist = json_decode(file_get_contents("hist.json"), true);
    $new_hist[$entree] = array('poids' => $new["historique"][$entree]["sommes"]["total"]["poids"], 'panier' => $new["historique"][$entree]["sommes"]["total"]["panier"]);
    file_put_contents("hist.json",json_encode($new_hist, JSON_UNESCAPED_UNICODE));
    echo 'OK';
}
catch (Exception $e)
{
    echo 'Erreur: ' . $e->getMessage() . "\n";
}

?>
