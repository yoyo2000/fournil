<?
// FUNCTIONS
function now(){
    // Retourne une chaine du genre "2018-12-16T07-20-33"
    date_default_timezone_set('Europe/Paris');
    return date('Y-m-d\TH-i-s');
}

function now2(){
    // Retourne une chaine du genre "2019-12-16"
    date_default_timezone_set('Europe/Paris');
    return date('Y-m-d');
}

function scan_dossier($d){
    // Retourne la liste (array) des fichiers du dossier $d
    $fl = scandir($d);
    array_shift($fl);    // retire le point
    array_shift($fl);    // retire le point point
    return $fl;
}

function backup_datas(){
    // Sauve le fichier $name dans le dossier $dossier avec une rotation sur $max fichiers
    $dossier = 'sav';
    $name = 'datas.json';
    $max = 20;

    if(!is_dir($dossier)){
        mkdir($dossier);
    }

    copy($name, $dossier.'/f-'.now().'.json');

    $files = scan_dossier($dossier);
    if (count($files) > $max){
        if (file_exists($dossier.'/'.$files[0])){
            unlink($dossier.'/'.$files[0]); 
        }
    }
}

function backup_hist(){
    // Sauve le fichier $name dans le dossier $dossier avec une rotation sur $max fichiers
    // Une seule copie par jour et ne copie que si la taille a changé
    $dossier = 'hist';
    $name = 'hist.json';
    $max = 20;
    $name_copy = 'h-'.now2().'.json';

    if(!is_dir($dossier)){
        mkdir($dossier);
    }

    if (!file_exists($dossier.'/'.$name_copy)){ // Si un fichier avec la même date existe déjà on ne fait rien

        $files = scan_dossier($dossier);
        if (count($files) >= 1){
            $lastFile = $files[count($files) - 1];  // Nom du dernier fichier sauvegardé
            if (filesize($name) == filesize($dossier.'/'.$lastFile)){
                return; // On ne fait rien non plus si la taille du dernier fichier est identique au nouveau
            }
        }

        copy($name, $dossier.'/'.$name_copy);

        if (count($files) > $max){
            if (file_exists($dossier.'/'.$files[0])){
                unlink($dossier.'/'.$files[0]); 
            }
        }
    }
}

// START
backup_datas();
backup_hist();
?>