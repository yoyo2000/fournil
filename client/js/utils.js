"use strict";

String.prototype.capitalize = function () {
	// Met en majuscule la 1ère lettre
	return this.charAt(0).toUpperCase() + this.substr(1);
}

var f_utils = (function () {
	// Private Members
	var gloutons;
	var pains;
	var fournee;
	var tirelire;
	var historique;
	var listeJours_d = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
	var mm = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
	var tmp_datas_fournee = { "gloutons": {} };
	var tmp_id;
	const INIT_POIDS_TYPE = () => { return { "p": 0, "s": 0, "g": 0, "e": 0, "b": 0 } }; // Type de pain = 1ère lettre du code dans objet pains

	// Public Members
	return {

		"charger": function () {
			gloutons = datas.gloutons;
			pains = datas.pains;
			fournee = datas.fourneeEnCours || { "version": 2, "date": "", "gloutons": [], "sommes": {}, "panier": {} };
			tirelire = datas.tirelire || { "001": 0, "002": 0, "005": 0, "01": 0, "02": 0, "05": 0, "1": 0, "2": 0, "5": 0, "10": 0, "20": 0, "50": 0, "100": 0, "200": 0, "500": 0, "cheque": 0 };
			historique = datas.historique || {};
			var vm_gloutons = new Vue({
				el: '#glouton_app',
				data: {
					gloutons: gloutons,
					pains: pains,
					fourneeEnCours: fournee,
					tirelire: tirelire,
					historique: historique,
					listeJours: ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"],
					typePains: { "p": "Froment", "g": "Engrain", "s": "Seigle", "e": "Épices", "b": "Biscuits" },
					jours: [],			// jours de fournées cochés dans paramètres
					gloutonEdit: [], 	// [ ancien_index, {glouton_en_édition} ]
					inputRecherche: '',
					inputRechercheAtt: '',
					inputNom: '',
					inputMail: '',
					inputTel: '',
					inputCom: {},
					inputDate: '',
					inputDateVacDeb: '',
					inputDateVacFin: '',
					inputSolde: '0',
					inputAjout: '',
					inputAnnot: '',
					sauvImpress: true,
					sauvValid: true,
					previsions: {}
				},
				computed: {
					listeActifs: function () {
						var rech = this.inputRecherche;
						var g = this.gloutons.slice();
						if (rech != '') {
							return g.filter(function (gl) {
								return (gl.nom.toLowerCase().indexOf(rech.toLowerCase()) >= 0) && !gl.attente
							});
						} else {
							return g.filter(function (gl) {
								return !gl.attente
							});
						}
					},
					listeEnAttente: function () {
						var rech = this.inputRechercheAtt;
						var g = this.gloutons.slice();
						if (rech != '') {
							return g.filter(function (gl) {
								return (gl.nom.toLowerCase().indexOf(rech.toLowerCase()) >= 0) && gl.attente
							});
						} else {
							return g.filter(function (gl) {
								return gl.attente
							});
						}
					},
					totalSoldes: function () {
						let tot = 0;
						for (var glouton of this.gloutons) {
							tot += glouton.solde;
						}
						return tot;
					},
				},
				created() {
					// Changement du title
					document.title += " - " + this.nomGen();
					// Initialisation de l'objet fourneeEnCours si aucun importé
					if (this.fourneeEnCours.date == "") this.razFourneeEnCours(true);
				},
				mounted() {
					if (localStorage.getItem("jours")) {
						try {
							this.jours = JSON.parse(localStorage.getItem('jours'));
						} catch (e) {
							localStorage.removeItem('jours');
						}
					}
					if (localStorage.getItem("sauvImpress")) {
						try {
							this.sauvImpress = JSON.parse(localStorage.getItem('sauvImpress'));
						} catch (e) {
							localStorage.removeItem('sauvImpress');
						}
					}
					if (localStorage.getItem("sauvValid")) {
						try {
							this.sauvValid = JSON.parse(localStorage.getItem('sauvValid'));
						} catch (e) {
							localStorage.removeItem('sauvValid');
						}
					}
					$(this.$refs.GloutonModal).on("hidden.bs.modal", this.annulerEdit);
					$(this.$refs.CommandeModal).on("hidden.bs.modal", this.annulerEdit);
					$(this.$refs.GloutonModal).on("shown.bs.modal", this.focusNewNom);
				},
				watch: {
					jours: function (newJours) {
						const parsed = JSON.stringify(newJours);
						localStorage.setItem('jours', parsed);
					},
					sauvImpress: function (newVal) {
						localStorage.setItem('sauvImpress', newVal);
					},
					sauvValid: function (newVal) {
						localStorage.setItem('sauvValid', newVal);
					}
				},
				methods: {
					calculPrevisions: function () {
						let prev = { "rec": {}, "ponc": {} };
						let cumulpoidsrec = {};	// Mémorise le poids des commandes récurrentes pour pouvoir faire le calcul des commandes ponctuelles ("rien")
						let p = this.pains;		// Passage par une variable sinon this.pains n'est pas accessible dans les sous boucles each et for
						let isJ = this.testIsCommandeRec;	// Idem pour la fonction de test des types de commandes
						let isOld = this.testIsOldCommande;	// Idem pour la fonction de test de commandes passées
						// Ajout des clés pour les jours
						$.each(this.jours, function (i, j) {
							prev.rec[j] = INIT_POIDS_TYPE();
							prev.ponc[j] = {};
						});
						// Parcours des gloutons actifs pour cumuler les poids des commandes récurrentes
						for (const glouton of this.listeActifs) {
							let listeCom = Object.keys(glouton.commande);
							// Parcours des jours de fournées récurrentes
							$.each(Object.keys(prev.rec), function (i, j) {
								if (listeCom.indexOf(j) > -1) {
									if (!cumulpoidsrec.hasOwnProperty(glouton.nom)) {
										cumulpoidsrec[glouton.nom] = {};
									}
									cumulpoidsrec[glouton.nom][j] = INIT_POIDS_TYPE();
									// Parcours des pains de la commande
									for (var pain in glouton.commande[j]) {
										cumulpoidsrec[glouton.nom][j][pain.slice(0, 1)] += (glouton.commande[j][pain] * (p[pain].poids * 1000)) / 1000;
									};
									// Parcours des types de pains
									for (const type in prev.rec[j]) {
										prev.rec[j][type] += cumulpoidsrec[glouton.nom][j][type];
									}
								}
							});
						}
						// Parcours des gloutons actifs pour cumuler les poids des commandes ponctuelles
						for (const glouton of this.listeActifs) {
							let listeCom = Object.keys(glouton.commande);
							// Parcours des commandes ponctuelles et parmi les jours de fournée paramétrés
							$.each(listeCom, function (i, j) {
								if (!isJ(j) && !isOld(j)) { // commande ponctuelle et non passée
									let j_sem = listeJours_d[new Date(j).getDay()]; // jour de la semaine de la commande
									if (Object.keys(prev.rec).indexOf(j_sem) > -1) { // Commande dans les jours paramétrés
										let cumulpoids = INIT_POIDS_TYPE();
										// Parcours des pains de la commande
										for (var pain in glouton.commande[j]) {
											cumulpoids[pain.slice(0, 1)] += (glouton.commande[j][pain] * (p[pain].poids * 1000)) / 1000;
										};
										if (!prev.ponc[j_sem].hasOwnProperty(j)) {
											prev.ponc[j_sem][j] = { ...prev.rec[j_sem] };	// initialisé avec le poids des commandes récurrentes
										}
										let cumulpoidsrec_g = INIT_POIDS_TYPE();	// Cumul de commande récurrente pour ce jour et ce glouton
										if (cumulpoidsrec.hasOwnProperty(glouton.nom)) {
											// Les gloutons peuvent avoir des commandes ponctuelles sans commandes récurrentes
											if (cumulpoidsrec[glouton.nom].hasOwnProperty(j_sem)) {
												// Il faut une commande récurrente correspondant à ce jour de commande ponctuelle
												cumulpoidsrec_g = { ...cumulpoidsrec[glouton.nom][j_sem] };
											}
										}
										// Parcours des types de pains
										for (const type in prev.ponc[j_sem][j]) {
											prev.ponc[j_sem][j][type] += (cumulpoids[type] * 1000 - cumulpoidsrec_g[type] * 1000) / 1000;	// poids ponctuel moins poids récurrent												
										}
									}
								}
							});
						}
						this.previsions = { ...prev };
					},
					mettreEnAttenteGlouton: function (name) {
						// Bouton pour mettre en attente un glouton actif
						var index = this.trouveIndexGlouton(name);
						if (index !== -1) {
							// la prop "attente" peut être initialement absente => "set" pour la rendre réactive
							// voir https://fr.vuejs.org/v2/guide/list.html#Limitation-de-detection-de-changement-dans-un-objet
							this.$set(this.gloutons[index], 'attente', true);
							this.calculeListeGloutonsFourneeEnCours();
						}
					},
					retablirGlouton: function (name) {
						// Bouton pour rendre actif un glouton en attente
						var index = this.trouveIndexGlouton(name);
						if (index !== -1) {
							this.gloutons[index].attente = false;
							this.calculeListeGloutonsFourneeEnCours();
							if (this.listeEnAttente.length == 0) {
								$("#gloutons-link a").click();
							}
						}
					},
					eliminerGlouton: function (name) {
						// Bouton de suppression d'un glouton
						if (confirm("Éliminer le glouton " + name + " ?")) {
							var index = this.trouveIndexGlouton(name);
							if (index !== -1) {
								this.gloutons.splice(index, 1);
								if (this.listeEnAttente.length == 0) {
									$("#gloutons-link a").click();
								}
							}
						}
					},
					toutRetablir: function () {
						// Bouton pour rendre actif tous les gloutons en attente
						if (confirm("Rétablir tous les gloutons en attente ?")) {
							this.listeEnAttente.map(function (gl) {
								gl.attente = false
							});
							$("#gloutons-link a").click();
						}
					},
					arrondiCalcul: function (val) {
						// Retourne une valeur arrondie à 2 chiffres à partir d'un float
						if (!val) return 0;
						if (typeof (val) == "string") val = parseFloat(val);
						let x = parseFloat(val.toFixed(2));
						return isNaN(x) ? 0 : x;
					},
					affichePrix: function (prix) {
						// Retourne un prix formaté à partir d'un float
						// 125665.789 returns "125 665,79 €"
						// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
						return new Intl.NumberFormat('fr-FR', {
							style: 'currency',
							currency: 'EUR'
						}).format(prix);
					},
					viderModal: function () {
						// Vide les champ d'ajout / modif de glouton
						this.inputNom = "";
						this.inputMail = "";
						this.inputTel = "";
						this.inputDate = "";
						this.inputDateVacDeb = "";
						this.inputDateVacFin = "";
						this.inputSolde = "0";
						this.inputCom = {};
						this.inputAnnot = "";
						// Réinitialise les min et max des dates de vacances
						$('#inputDateVac2').attr('min', this.formatCommandeDate(new Date()));
						$('#inputDateVac1').attr('max', 0);
					},
					remplirModal: function (index) {
						// Charge les champ d'ajout / modif de glouton et soustrait ce glouton du tableau global
						// index : index du glouton à gérer dans la liste de tous les gloutons
						this.inputNom = this.gloutons[index].nom;
						this.inputMail = this.gloutons[index].mail;
						this.inputTel = this.gloutons[index].tel;
						this.inputSolde = this.gloutons[index].solde.toString();
						this.inputCom = JSON.parse(JSON.stringify(this.gloutons[index].commande)); // Deep object copy : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Deep_Clone
						this.inputAnnot = this.gloutons[index].annot;
						// Soustrait ce glouton du tableau global et le conserve en cas d'annulation de la modification
						this.gloutonEdit.push(index); // Conserve l'ancien index
						this.gloutonEdit.push(this.gloutons.splice(index, 1)[0]);
					},
					ajouterGlouton: function () {
						// Bouton d'ajout d'un glouton
						this.viderModal();
						$('#GloutonModal').modal('show')
					},
					modifierGlouton: function (name) {
						// Bouton de modification des informations d'un glouton
						var index = this.trouveIndexGlouton(name);
						if (index !== -1) {
							this.remplirModal(index)
							$('#GloutonModal').modal('show');
						}
					},
					exclureGloutonFournee: function (name, date) {
						// Bouton d'exclusion d'un glouton de la fournée en cours
						// => ajout d'une commande ponctuelle vide pour la date de la fournée en cours
						if (confirm("Exclure le glouton " + name + " de la fournée en cours ?")) {
							var index = this.trouveIndexGlouton(name);
							if (index !== -1) {
								Vue.set(this.gloutons[index].commande, date, {});
								this.calculeListeGloutonsFourneeEnCours();
							}
						}
					},
					imprimerGloutonOuPas: function (event) {
						// Bouton d'ajout ou d'exclusion d'un glouton de l'impression de la fournée en cours
						$(event.target.parentNode).toggleClass("table-light d-print-none");
					},
					imprimerToutOuRien: function (event) {
						// Bouton d'ajout ou d'exclusion de tous les gloutons de l'impression de la fournée en cours
						if ($("#tr-fourneeencours-0").hasClass("table-light")) {
							$(event.target.parentNode).parents("table").find("tbody tr").removeClass("table-light d-print-none");
						} else {
							$(event.target.parentNode).parents("table").find("tbody tr").addClass("table-light d-print-none");
						}
					},
					modifierCom: function (name) {
						// Bouton de modification des commandes d'un glouton
						var index = this.trouveIndexGlouton(name);
						if (index !== -1) {
							this.remplirModal(index)
							$('#CommandeModal').modal('show');
						}
					},
					annulerEdit: function () {
						// Fermeture de la fenetre modale d'ajout / modif de glouton
						if (this.gloutonEdit.length !== 0) {
							// Restitue le glouton en édition avec son ancien index
							this.gloutons.splice(this.gloutonEdit[0], 0, this.gloutonEdit[1]);
						}
						this.gloutonEdit = [];
						this.viderModal();
						$('#GloutonModal').modal('hide');
						$('#CommandeModal').modal('hide');
					},
					focusNewNom: function () {
						// Ouverture de la fenetre modale d'édition
						this.$refs.newNom.focus(); // Curseur dans le champ nom
					},
					toggleBoutonVac: function () {
						// Bouton affichant le formulaire de saisie de vacances et masquant les boutons de saisie de commandes
						$("#btnAddVac").toggleClass("btn-success");
						$("#btnAddVac").toggleClass("btn-outline-success");
					},
					changeMinInputVac2: function () {
						// Place la date "Début" + 1 jour comme date minimale dans le champ "Fin" du formulaire de saisie de vacances
						let DateVac1 = $("#inputDateVac1").val();
						let min;
						if (DateVac1 !== "") {
							min = new Date($("#inputDateVac1").val());
							min.setDate(min.getDate() + 1);
						} else {
							// Date du jour comme minimum en cas de reset de la date de début
							min = new Date();
						}
						$('#inputDateVac2').attr('min', this.formatCommandeDate(min));
					},
					changeMaxInputVac1: function () {
						// Place la date "Fin" comme date maximale dans le champ "Début" du formulaire de saisie de vacances
						let DateVac2 = $("#inputDateVac2").val();
						if (DateVac2 !== "") {
							let max = new Date(DateVac2);
							$('#inputDateVac1').attr('max', this.formatCommandeDate(max));
						} else {
							// Pas de maximum en cas de reset de la date de fin
							$('#inputDateVac1').attr('max', 0);
						}
					},
					ajouterCommandesVacances: function () {
						// Ajoute les commandes vides correspondantes aux dates de vacances saisies
						// Se base sur les commandes récurrentes du glouton en cours
						let listeComRec = Object.keys(this.inputCom).filter(this.testIsCommandeRec);
						if (listeComRec === []) { return; }  // Pas de commandes récurrentes donc pas de calcul possible

						if ($("#inputDateVac1").val() !== "" && $("#inputDateVac2").val() !== "") {
							let dateDebut = new Date($("#inputDateVac1").val());
							let dateFin = new Date($("#inputDateVac2").val());
							while (dateDebut <= dateFin) {
								// Parcours des dates de vacances
								if ((this.formatCommandeDate(dateDebut) in this.inputCom)) {
									// Date déjà dans la liste des commandes ponctuelles
									Vue.set(this.inputCom, this.formatCommandeDate(dateDebut), {});
								} else if (listeComRec.indexOf(listeJours_d[dateDebut.getDay()]) > -1) {
									// Jour correspondant à une date de commande récurrente
									Vue.set(this.inputCom, this.formatCommandeDate(dateDebut), {});
								}
								dateDebut = new Date(dateDebut.setDate(dateDebut.getDate() + 1));
							}
						}
					},
					valideAjoutSolde: function (event) {
						// Touche entrée dans un champ Ajout de la fenetre d'ajout / modif de glouton
						// => ajoute la valeur saisie au solde du glouton et vide le champs
						// let soldeAvantAjout = this.arrondiCalcul($("#gsolde").val());
						// $("#gsolde").val(soldeAvantAjout + this.arrondiCalcul(event.target.value));
						// event.target.value = '';

						this.inputSolde = (this.arrondiCalcul(this.inputSolde) + this.arrondiCalcul(this.inputAjout)).toString();
						this.inputAjout = '';
					},
					enregistrer: function () {
						// Bouton enregistrer de la fenetre d'ajout / modif de glouton
						// Vérification des commandes (pas de commandes récurrentes vide)
						for (var jour in this.inputCom) {
							if (this.testObjetVide(this.inputCom[jour]) & this.testIsCommandeRec(jour)) {
								Vue.delete(this.inputCom, jour);
							}
						};
						let val = this.inputSolde.replace(',', '.');
						var newG = {
							nom: this.inputNom,
							mail: this.inputMail,
							tel: this.inputTel,
							solde: this.arrondiCalcul(val),
							commande: this.inputCom,
							annot: this.inputAnnot
						};
						if (this.gloutonEdit.length !== 0) {
							// Met à jour et restitue le glouton en édition avec son ancien index
							$.extend(this.gloutonEdit[1], newG);
							this.gloutons.splice(this.gloutonEdit[0], 0, this.gloutonEdit[1]);
						} else {
							if (this.trouveIndexGlouton(newG.nom) > -1) {
								alert("Ce glouton existe déjà !");
								return 0;
							}
							this.gloutons.push(newG);
						}
						this.gloutonEdit = [];
						this.viderModal();
						$('#GloutonModal').modal('hide');
						$('#CommandeModal').modal('hide');
						// Met à jour les commandes de la fournée en cours
						this.calculeListeGloutonsFourneeEnCours();
					},
					updateNbArticle: function (obj, prop, event) {
						// Modifie le nombre d'article sinon l'affichage n'est pas pris en compte
						Vue.set(obj, prop, parseInt(event.target.value));
					},
					updatePanier: function (prop, event) {
						// Modifie le nombre des billets/pièces/chèques dans le panier de la fournée en cours
						// Pour les chèques, si la saisie commence par un "+" on ajoute la somme au total déjà saisi
						let chq = event.target.value, val;
						if (prop == "cheque") {
							if (chq.startsWith("+")) {
								// Ajout de la valeur saisie au total précédent
								val = this.arrondiCalcul(chq.replace(',', '.').replace('-', '').replace(/[  ]/g, '').replace('€', '')) + this.fourneeEnCours.panier.cheque;
							} else {
								// Remplacement du total
								val = chq.replace(',', '.').replace('-', '').replace(/[  ]/g, '').replace('€', '');
							}
							Vue.set(this.fourneeEnCours.panier, prop, this.arrondiCalcul(val));
						} else {
							Vue.set(this.fourneeEnCours.panier, prop, this.arrondiCalcul(chq));
						}
						this.majTotalPanier();
					},
					updateTirelire: function (prop, event) {
						// Modifie le nombre des billets/pièces/chèques dans la tirelire
						// Pour les chèques, si la saisie commence par un "+" on ajoute la somme au total déjà saisi
						let chq = event.target.value, val;
						if (prop == "cheque") {
							if (chq.startsWith("+")) {
								// Ajout de la valeur saisie au total précédent
								val = this.arrondiCalcul(chq.replace(',', '.').replace('-', '').replace(/[  ]/g, '').replace('€', '')) + this.tirelire[prop];
							} else {
								// Remplacement du total
								val = chq.replace(',', '.').replace('-', '').replace(/[  ]/g, '').replace('€', '');
							}
							this.tirelire[prop] = this.arrondiCalcul(val);
						} else {
							this.tirelire[prop] = this.arrondiCalcul(chq);
						}
					},
					modifieValeurPanier: function (prop, delta) {
						// Incrémente ou diminue le nombre des billets/pièces/chèques dans le panier de la fournée en cours (ajoute delta)
						let new_val = this.fourneeEnCours.panier[prop] + delta >= 0 ? this.fourneeEnCours.panier[prop] + delta : 0;
						Vue.set(this.fourneeEnCours.panier, prop, new_val);
						this.majTotalPanier();
					},
					modifieValeurTirelire: function (prop, delta) {
						// Incrémente ou diminue le nombre des billets/pièces/chèques dans la tirelire (ajoute delta)
						let new_val = this.tirelire[prop] + delta >= 0 ? this.tirelire[prop] + delta : 0;
						this.tirelire[prop] = new_val;
					},
					razTirelire: function () {
						// Remet à zéro le nombre de tous les billets/pièces/chèques de la tirelire
						if (confirm("Voulez-vous vraiment remettre à zéro la tirelire ?")) {
							for (var val in this.tirelire) {
								this.tirelire[val] = 0;
							};
						}
					},
					calculTotalTirelire: function () {
						// Calcule la somme totale des billets/pièces/chèques de la tirelire
						var tot = 0;
						for (var val in this.tirelire) {
							var multiplier = 1;	// valeur par défaut pour cheque
							if (val.indexOf("00") == 0) {	// 001 002 005
								multiplier = this.arrondiCalcul(0.01 * parseInt(val));
							} else if (val.indexOf("0") == 0) { // 01 02 05
								multiplier = this.arrondiCalcul(0.1 * parseInt(val));
							} else if (val != "cheque") { // 1 2 5 10 20 50 100 200 500
								multiplier = parseInt(val);
							}
							tot += this.arrondiCalcul(this.tirelire[val] * multiplier);
						};
						return this.arrondiCalcul(tot);
					},
					ajouterCommandeRec: function (jour, event) {
						// Ajoute une commande récurrente dans la fenetre d'ajout / modif de glouton
						if (!(jour in this.inputCom)) {
							Vue.set(this.inputCom, jour, {});
						}
					},
					ajouterCommandePonc: function (event) {
						// Ajoute une commande ponctuelle dans la fenetre d'ajout / modif de glouton
						if ((this.inputDate)) {
							Vue.set(this.inputCom, this.inputDate, this.renvoieArticlesJour(this.inputDate));
						}
					},
					ajouterCommandePoncFourneeEnCours: function (event) {
						// Ajoute une commande ponctuelle pour la fournée en cours dans la fenetre d'ajout / modif de glouton
						if (this.testFourneeEnCours()) {
							Vue.set(this.inputCom, this.fourneeEnCours.date, this.renvoieArticlesJour(this.fourneeEnCours.date));
						}
					},
					// ajouterCommandePoncProchain: function (jour, event) {
					// 	// Ajoute une commande ponctuelle au prochain "jour" dans la fenetre d'ajout / modif de glouton
					// 	var prochaineDate = this.trouveDateProchainJour(jour);
					// 	if (!(prochaineDate in this.inputCom)) {
					// 		Vue.set(this.inputCom, prochaineDate, this.renvoieArticlesJour(prochaineDate));
					// 	}
					// },
					renvoieArticlesJour: function (jour) {
						// Renvoie les articles correspondants au jour "jour" de commande récurrente ou objet vide sinon
						// jour est de la forme "2018-12-16"
						try {
							var date = new Date(jour);
							var ceJour = listeJours_d[date.getDay()];
							if (ceJour in this.inputCom) {
								return JSON.parse(JSON.stringify(this.inputCom[ceJour])); // Deep object copy : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Deep_Clone
							} else {
								return {};
							}
						} catch (error) {
							return {};
						}
					},
					razFourneeEnCours: function (razDate) {
						// Remet l'objet fourneeEnCours à 0
						this.fourneeEnCours.version = 2;
						// Si razDate est à true, effacement de la date
						if (razDate) {
							this.fourneeEnCours.date = "";
							tmp_datas_fournee = { "gloutons": {} };
						}
						this.fourneeEnCours.gloutons = [];
						var sommes = {
							"total": {
								"poids": INIT_POIDS_TYPE(),
								"prix": 0,
								"versement": 0,
								"panier": 0
							}
						};
						for (var pain in this.pains) {
							sommes[pain] = {
								"nombre": 0,
								"prix": 0,
								"poids": 0
							};
						};
						var panier = tmp_datas_fournee.panier || {
							"001": 0,
							"002": 0,
							"005": 0,
							"01": 0,
							"02": 0,
							"05": 0,
							"1": 0,
							"2": 0,
							"5": 0,
							"10": 0,
							"20": 0,
							"50": 0,
							"100": 0,
							"200": 0,
							"500": 0,
							"cheque": 0
						};
						Vue.set(this.fourneeEnCours, "sommes", sommes);
						Vue.set(this.fourneeEnCours, "panier", panier);
					},
					clickFournee: function (event) {
						// Click sur l'onglet "Nouvelle Fournée" ou "Fournée en cours"
						// Si aucune fournée en cours alors en génère une
						if (!this.testFourneeEnCours()) {
							if (this.jours.length == 0) {
								// Si aucuns jours de fournée paramétrés => messages + renvoi à l'onglet paramètre
								alert("Vous devez d'abord paramétrer vos jours de fournée dans l'onglet 'Paramètres' !");
								event.stopPropagation();	// Stop l'ouverture de l'onglet fournée
								Vue.nextTick(function () {
									vm_gloutons.$refs.tabParams.click();
								});
							} else {
								this.genereNouvelleFournee();
							}
						}
					},
					genereNouvelleFournee: function () {
						// Génère une nouvelle fournée à partir des jours de fournée paramétrés et des gloutons actifs
						if (this.jours.length == 0) {
							// Si aucuns jours de fournée paramétrés => messages + renvoi à l'onglet paramètre
							alert("Vous devez d'abord paramétrer vos jours de fournée dans l'onglet 'Paramètres' !");
						} else {
							var dateNouvelleFournee = this.trouveDateNouvelleFournee();
							this.fourneeEnCours.date = this.formatCommandeDate(dateNouvelleFournee);
							this.calculeListeGloutonsFourneeEnCours();
						}
					},
					genereFourneeAncienne: function (ancienneDate) {
						// Génère une fournée pour une date passée (debug)
						if (this.fourneeEnCours.date !== "") {
							// Si une fournée est déjà en cours => erreur
							alert("Impossible de générer une ancienne fournée car une autre est actuellement en cours !");
						} else {
							this.fourneeEnCours.date = ancienneDate; // format : "2018-12-16"
							this.calculeListeGloutonsFourneeEnCours();
						}
					},
					calculeListeGloutonsFourneeEnCours: function () {
						// Calcul des gloutons concernés par la fournée en cours (au moins une commande)
						if (!this.testFourneeEnCours()) return 0;	// Aucun calcul si pas de fournée en cours
						// Sauvegarde des versements et du panier déjà saisi
						tmp_datas_fournee.panier = JSON.parse(JSON.stringify(this.fourneeEnCours.panier))
						for (var i in this.fourneeEnCours.gloutons) {
							if (this.fourneeEnCours.gloutons[i].verse !== "") {
								tmp_datas_fournee.gloutons[this.fourneeEnCours.gloutons[i].nom] = this.fourneeEnCours.gloutons[i].verse;
							}
						}
						this.razFourneeEnCours(false); 	// Vidage des données de la fournée sauf la date
						var rech = this.inputRecherche;	// Sauvegarde de la recherche en cours
						this.viderRecherche();	// Sinon la fournée est calculée à partir du résultat de la recherche
						var jourFournee = listeJours_d[new Date(this.fourneeEnCours.date).getDay()];
						var listeGlouton = [];
						for (const glouton of this.listeActifs) {
							var listeCom = Object.keys(glouton.commande);
							var jourCom;
							// Les commandes ponctuelles sont prioritaires
							if (listeCom.indexOf(this.fourneeEnCours.date) > -1) {
								jourCom = this.fourneeEnCours.date;
							} else if (listeCom.indexOf(jourFournee) > -1) {
								jourCom = jourFournee;
							} else {
								continue;
							}
							if (!this.testObjetVide(glouton.commande[jourCom])) { // La commande de ce jour peut être vide
								let sommedue = 0;
								for (var pain in glouton.commande[jourCom]) {
									sommedue += glouton.commande[jourCom][pain] * this.pains[pain].prix;
									this.fourneeEnCours.sommes[pain]["nombre"] += glouton.commande[jourCom][pain];
									this.fourneeEnCours.sommes[pain]["prix"] += this.arrondiCalcul(glouton.commande[jourCom][pain] * this.pains[pain].prix);
									this.fourneeEnCours.sommes[pain]["poids"] += (glouton.commande[jourCom][pain] * (this.pains[pain].poids * 1000)) / 1000;
									this.fourneeEnCours.sommes.total.poids[pain.slice(0, 1)] += (glouton.commande[jourCom][pain] * (this.pains[pain].poids * 1000)) / 1000;
									this.fourneeEnCours.sommes.total.prix += this.arrondiCalcul(glouton.commande[jourCom][pain] * this.pains[pain].prix);
								};
								let verse = "";
								if (typeof (tmp_datas_fournee.gloutons[glouton.nom]) !== "undefined") {
									verse = tmp_datas_fournee.gloutons[glouton.nom];
								}
								listeGlouton.push({
									"nom": glouton.nom,
									"solde": glouton.solde,
									"articles": glouton.commande[jourCom],
									"annot": glouton.annot,
									"verse": verse,
									"due": this.arrondiCalcul(sommedue)
								});
							}
						};
						this.fourneeEnCours.sommes.total.prix = this.arrondiCalcul(this.fourneeEnCours.sommes.total.prix);
						this.fourneeEnCours.gloutons = listeGlouton.sort(this.triGloutonsNom);
						this.majVersement();
						this.majTotalPanier();
						this.inputRecherche = rech; // Restitue la recherche en cours
					},
					ignorerFournee: function () {
						// Clic sur le bouton ignorer fournée
						if (confirm("Voulez-vous vraiment ignorer la fournée du " + this.formatJolieDate(this.fourneeEnCours.date) + " ?")) {
							this.razFourneeEnCours(true);
							this.$refs.tabGloutonsActifs.click();
						}
					},
					imprimerFournee: function () {
						// Clic sur le bouton imprimer fournée
						window.print();
						// Sauvegarde auto
						if (this.sauvImpress) {
							f_utils.envoyer();
						}
					},
					validerFournee: function () {
						// Clic sur le bouton valider fournée
						// Vérification de la saisie des versements (le champ ne doit pas être vide)
						for (var i in this.fourneeEnCours.gloutons) {
							if (this.fourneeEnCours.gloutons[i].verse === "") {
								alert("La saisie des versements des gloutons n'est pas complète.\nImpossible de continuer.");
								return 0;
							}
						}

						// Vérification que panier=versement global
						if (this.fourneeEnCours.sommes.total.versement != this.fourneeEnCours.sommes.total.panier) {
							if (!confirm("Le panier et les versements des gloutons sont différents, continuer ?")) {
								return 0;
							}
						}

						// Basculer les billets/pièces/chèques du panier vers la tirelire
						for (var val in this.fourneeEnCours.panier) {
							this.tirelire[val] += this.fourneeEnCours.panier[val];
						};

						// Parcours des gloutons de la fournée pour maj du solde selon la somme dûe/versée
						for (var i in this.fourneeEnCours.gloutons) {
							var index_g = this.trouveIndexGlouton(this.fourneeEnCours.gloutons[i].nom);
							if (index_g !== -1) {
								this.gloutons[index_g].solde += this.arrondiCalcul(this.fourneeEnCours.gloutons[i].verse - this.fourneeEnCours.gloutons[i].due);
							}
						}
						// Parcours de tous les gloutons pour suppression des commandes ponctuelles correspondant à la fournée en cours
						for (var glouton of this.gloutons) {
							var listeCom = Object.keys(glouton.commande);
							if (listeCom.indexOf(this.fourneeEnCours.date) > -1) {
								Vue.delete(glouton.commande, this.fourneeEnCours.date);;
							}
						}

						// Archivage et réinitialisation
						const f_p = JSON.stringify(this.fourneeEnCours).slice(0, -1) + ', "pains":' + JSON.stringify(this.pains) + '}'	// Ajout des pains à la fournée pour archivage (version 2)
						this.historique[this.fourneeEnCours.date] = JSON.parse(f_p);
						this.razFourneeEnCours(true);
						this.$refs.tabGloutonsActifs.click();

						// Sauvegarde auto
						if (this.sauvValid) {
							f_utils.envoyer();
						}
					},
					formatJolieDate: function (jour) {
						// Convertit un jour "2018-12-16" en jolie date formatée "Dimanche 16 décembre 2018"
						try {
							var date = new Date(jour);
							return listeJours_d[date.getDay()].capitalize() + " " + date.getDate() + " " + mm[date.getMonth()] + " " + date.getFullYear();
						} catch (error) {
							return jour;
						}
					},
					formatCommandeDate: function (date) {
						// Convertit un objet date en chaine formatée "2018-12-16"
						return date.toISOString().slice(0, 10);
					},
					// todayLocal: function () {
					// 	// Renvoie la date du jour à l'heure locale (objet Date)
					// 	let d_gmt = new Date();
					// 	let offset = d_gmt.getTimezoneOffset() / -60;
					// 	return new Date(d_gmt.setHours(d_gmt.getHours() + offset));
					// },
					trouveDateProchainJour: function (jour) {
						// Trouve la date du prochain "jour"
						// Si on est Dimanche 16 décembre 2018 alors :
						// trouveDateProchainJour("lundi") => "2018-12-17"
						// trouveDateProchainJour("dimanche") => "2018-12-23"
						var date = new Date(); //this.todayLocal();
						var numJour = listeJours_d.indexOf(jour);
						var resultDate = new Date(date.getTime());
						resultDate.setUTCDate(date.getUTCDate() + (7 + numJour - date.getUTCDay() - 1) % 7 + 1);
						return this.formatCommandeDate(resultDate);
					},
					trouveDateNouvelleFournee: function () {
						// Détermine la date de la nouvelle fournée à partir des jours de fournée entrés en paramètre
						// j=0 => dimanche
						// Retourne un objet Date
						var jour = new Date(); //this.todayLocal();
						var jours_index = [];
						$.each(this.jours, function (i, j) {
							jours_index.push(listeJours_d.indexOf(j));
						});
						while (!jours_index.includes(jour.getUTCDay())) { // On recherche le 1er jour suivant correspondant aux fournées définies
							jour.setUTCDate(jour.getUTCDate() + 1);
						}
						return jour;
					},
					ecritVersement: function (i, event) {
						// Écrit la valeur de versement de ce glouton dans la fournée en cours
						let val = event.target.innerText.trim().replace(',', '.').replace('-', '').replace(/[  ]/g, '').replace('€', ''); // Attention c'est pas un vrai espace à remplacer
						Vue.set(this.fourneeEnCours.gloutons[i], "verse", this.arrondiCalcul(val));
						event.target.innerText = this.affichePrix(this.fourneeEnCours.gloutons[i].verse);
						$(event.target.parentNode).removeClass("table-active");
						this.majVersement();
					},
					activeVersement: function (event) {
						// Un champ de saisie de versement reçoit le focus => coloration de toute la ligne du tableau + selection du texte
						$(event.target.parentNode).addClass("table-active");
						this.selectionner(event.target);
					},
					valideVersement: function () {
						// Touche entrée dans un champ de saisie de versement => passe le focus au champ suivant
						var $listeChamps = $('#GloutonsFourneeEnCours td[contenteditable]');
						var index = $listeChamps.index(document.activeElement) + 1;
						if (index >= $listeChamps.length) {
							$("#toggle-recap-panier").focus();
							$('#btn_Top').click();
						} else {
							$listeChamps.eq(index).focus();
						}
					},
					valideChamp: function (pattern) {
						// Touche entrée dans un champ de saisie => passe le focus au champ suivant et reboucle au début
						// pattern permet de définir la liste des champs sur lesquels boucler
						var $listeChamps = $(pattern);
						var index = $listeChamps.index(document.activeElement) + 1;
						if (index >= $listeChamps.length) {
							$listeChamps.eq(0).focus();
						} else {
							$listeChamps.eq(index).focus();
						}
					},
					valideCheque: function (event, pattern) {
						// Touche entrée dans un champ de saisie de chèque => valide la saisie (perte du focus pour déclencher l'événement blur)
						// Si ajout (commence par un "+") on remet le focus sinon on passe le focus au champ suivant (selon pattern)
						let chq = event.target.value;
						if (chq.startsWith("+")) {
							// Ajout d'une valeur donc perte puis récupération du focus
							$("#btn-send-json").focus();
							tmp_id = event.target.id;
							Vue.nextTick(function () {
								$("#" + tmp_id).focus();
							});
						} else {
							// Remplacement du total donc passage du focus au champ suivant
							let $listeChamps = $(pattern);
							let index = $listeChamps.index(document.activeElement) + 1;
							$listeChamps.eq(index).focus();
						}
					},
					escCheque: function (event) {
						// Touche échap dans un champ de saisie de chèque => réécrit le total précédent et le sélectionne
						if (event.target.id === "chqpanier") {
							event.target.value = this.affichePrix(this.fourneeEnCours.panier.cheque);
						} else if (event.target.id === "chqtirelire") {
							event.target.value = this.affichePrix(this.tirelire["cheque"]);
						} else {
							return;
						}
						$("#btn-send-json").focus();
						tmp_id = event.target.id;
						Vue.nextTick(function () {
							$("#" + tmp_id).focus();
						});
					},
					ecritPrixPain: function (p_id, event) {
						// Écrit la valeur du prix d'un pain dans les paramètres
						let val = event.target.innerText.trim().replace(',', '.').replace('-', '').replace(/[  ]/g, '').replace('€', ''); // Attention c'est pas un vrai espace à remplacer
						Vue.set(this.pains[p_id], "prix", this.arrondiCalcul(val));
						this.calculeListeGloutonsFourneeEnCours();
					},
					majVersement: function () {
						// Met à jour la somme totale des versement dans la fournée en cours
						let tot = 0;
						for (var i in this.fourneeEnCours.gloutons) {
							if (this.fourneeEnCours.gloutons[i].verse !== "") {
								tot += this.arrondiCalcul(this.fourneeEnCours.gloutons[i].verse);
							}
						}
						Vue.set(this.fourneeEnCours.sommes.total, "versement", this.arrondiCalcul(tot));
					},
					majTotalPanier: function () {
						// Calcule et met à jour la somme totale des billets/pièces/chèques du panier de la fournée en cours
						var tot = 0;
						for (var val in this.fourneeEnCours.panier) {
							var multiplier = 1; // valeur par défaut pour cheque
							if (val.indexOf("00") == 0) { // 001 002 005
								multiplier = this.arrondiCalcul(0.01 * parseInt(val));
							} else if (val.indexOf("0") == 0) { // 01 02 05
								multiplier = this.arrondiCalcul(0.1 * parseInt(val));
							} else if (val != "cheque") { // 1 2 5 10 20 50 100 200 500
								multiplier = parseInt(val);
							}
							tot += this.arrondiCalcul(this.fourneeEnCours.panier[val] * multiplier);
						};
						Vue.set(this.fourneeEnCours.sommes.total, "panier", this.arrondiCalcul(tot));
					},
					ajouterArticle: function (jour, p_id, event) {
						// Ajoute un article de commande dans la fenetre d'ajout / modif de glouton
						if (!(p_id in this.inputCom[jour])) {
							Vue.set(this.inputCom[jour], p_id, 1);
						}
					},
					supprimerCommande: function (obj, prop, event) {
						// Supprime une commande dans la fenetre d'ajout / modif de glouton
						Vue.delete(obj, prop);
					},
					supprimerArticle: function (obj, prop, event) {
						// Supprime un article de commande dans la fenetre d'ajout / modif de glouton
						Vue.delete(obj, prop);
					},
					viderArticles: function (obj, event) {
						// Supprime tous les articles d'une commande dans la fenetre d'ajout / modif de glouton
						for (var article in obj) {
							Vue.delete(obj, article);
						}
					},
					viderRecherche: function () {
						// Vide le champ recherche des gloutons actifs
						this.inputRecherche = "";
						this.$refs.gRecherche.focus();
					},
					viderRechercheAtt: function () {
						// Vide le champ recherche des gloutons en attente
						this.inputRechercheAtt = "";
						this.$refs.gRechercheAtt.focus();
					},
					rien: function () {
						// Retourne un message pour dire que "c'est vide"
						var r = ["Rien", "Nada", "Walou", "Vide", "Néant", "Zéro", "Dépeuplé", "Déserté", "À sec", "Épuisé", "Liquidé", "Asséché", "Vidangé", "Siphonné", "Des clous", "Nul", "Misère", "Bernique", "Que dalle", "Peau de balles", "Des nèfles", "Macache", "Peau de zébi", "Circulez", "Rien à déclarer", "Sans objet", "Veuillez raccrocher", "Ça va couper", "Inexistant", "Repos", "Lessivé", "En grève", "Parlez après le bip", "Vous pouvez répéter la question ?", "Je crois que ça va pas être possible", "C'est du jamais vu", "Même pas en rève", "Houston, nous avons un problème", "MAYDAY MAYDAY", "La vérité est ailleurs", "Peace and love", "M'enfin", "Fermeture annuelle", "Cherche glouton perdu", "Y a quelqu'un ?", "Méoukilé ?", "Parti sans laisser d'adresse", "N'en parlons plus", "Aucun résultat", "Le compte est bon"];
						return r[Math.floor(Math.random() * r.length)];
					},
					nomGen: function () {
						// Génère un joli nom pour l'onglet des gloutons
						var sujet = ["Mâlinois", "Panivores", "Gastronomes", "Gloutons", "Gourmands", "Bourguignons", "Tartineurs", "Amis du bon pain", "Goinfres", "Fidèles bâfreurs", "Mangeurs de pain", "Joyeux bouffeurs", "Amateurs de pain", "Engloutisseurs"];
						var adj = ["mâlinois", "panivores", "gastronomes", "gloutons", "gourmands", "bourguignons", "dévoreurs de tartines", "ensorcelés", "pleins d'appétit", "voraces", "affamés", "insatiables", "connaisseurs", "ventrus", "éclairés"];
						var nb_idem = 7; // nombre de sujets/adjectifs incompatibles (ils ont le même indice)
						var sujet_i = Math.floor(Math.random() * sujet.length);
						var adj_i = Math.floor(Math.random() * adj.length);
						if (sujet_i < nb_idem) {
							while (sujet_i == adj_i) adj_i = Math.floor(Math.random() * adj.length);
						}
						return "Les " + sujet[sujet_i] + " " + adj[adj_i]
					},
					testObjetVide: function (obj) {
						// Renvoie true si un objet est vide => "{}"
						if ($.isEmptyObject(obj)) return true;
					},
					testFourneeEnCours: function () {
						// Renvoie true si une fournée est en cours
						// Teste la présence de la date de fournée
						if (this.fourneeEnCours.date != "") return true;
					},
					testIsCommandeRec: function (key) {
						// Renvoie true si une commande est récurrente
						// key : soit un jour de semaine donc commande récurrente, soit une date donc commande ponctuelle
						return (this.listeJours.indexOf(key) > -1);
					},
					testIsOldCommande: function (key) {
						// Renvoie true si une commande ponctuelle est passée
						// key : soit un jour de semaine pour une commande récurrente
						//       soit une date pour une commande ponctuelle ("2018-12-16")
						if (this.listeJours.indexOf(key) > -1) {
							return false;
						} else {
							try {
								var date = new Date(key);
								date.setUTCDate(date.getUTCDate() + 1);
								var today = new Date();
								return today > date;
							} catch (error) {
								return false;
							}
						}
					},
					trouveIndexGlouton: function (name) {
						// Retrouve l'index d'un glouton à partir de son nom
						return this.gloutons.findIndex(function (gl) {
							return gl.nom == name
						});
					},
					sendDropdown: function (id) {
						// Envoie un événement dropdown à l'objet du DOM dont on reçoit l'id
						$("#" + id).dropdown().dropdown('toggle');
					},
					triGloutonsNom: function (a, b) {
						// Fonction de tri des gloutons par nom
						return (a.nom.toUpperCase().localeCompare(b.nom.toUpperCase()))
					},
					triJours: function (a, b) {
						// Fonction de tri des jours de commande
						// Les éléments sont soit un jour de semaine ("lundi") soit une chaine représentant une date "2018-12-16"
						// Les jours de semaine doivent être triés et les dates aussi, avec jours > dates
						var isJ = this.testIsCommandeRec;
						if (isJ(a) & isJ(b)) {
							// Les 2 éléments sont des jours de semaine => tri selon la liste des jours
							return this.listeJours.indexOf(a) > this.listeJours.indexOf(b);
						} else if (!isJ(a) & !isJ(b)) {
							// Les 2 éléments sont des dates => tri alphabétique
							return a > b;
						} else if (!isJ(a) & isJ(b)) {
							// a=date et b=jour => les jours sont prioritaires
							return true;
						} else if (isJ(a) & !isJ(b)) {
							// a=jour et b=date => les jours sont prioritaires
							return false;
						}
					},
					selectionner: function (cible) {
						// Sélectionne le texte de la cible
						var doc = window.document, sel, range;
						sel = window.getSelection();
						range = doc.createRange();
						range.selectNodeContents(cible);
						sel.removeAllRanges();
						sel.addRange(range);
					},
					supprimeAnciennesCommandesPonctuelles: function () {
						// Parcours de tous les gloutons pour suppression des anciennes commandes ponctuelles
						for (var glouton of this.gloutons) {
							var listeCom = Object.keys(glouton.commande);
							for (var com of listeCom) {
								if (this.testIsOldCommande(com)) {
									console.log("Suppression pour ", glouton.nom, " de ", com);
									Vue.delete(glouton.commande, com);;
								}
							}
						}
					},
					capitalize: String.prototype.capitalize
				}

			});
		},

		"envoyer": function () {
			// Envoi des données au serveur
			var dataString = JSON.stringify({
				gloutons: gloutons,
				pains: pains,
				fourneeEnCours: fournee,
				tirelire: tirelire
			});
			$.ajax({
				url: 'json.php',
				data: {
					myData: dataString
				},
				type: 'POST'
			}).done(function (response) {
				if (response == "OK") {
					alert("Le fichier a bien été envoyé.");
				} else {
					alert("Le serveur a renvoyé l'erreur :\n", response);
				}
			}).fail(function (jqXHR, textStatus, error) {
				alert("Erreur lors de l'envoi du fichier datas :\n" + textStatus);
			});
			// Sauvegarde historique si besoin
			if ($.isEmptyObject(historique)) return;
			dataString = JSON.stringify({
				historique: historique
			});
			$.ajax({
				url: 'hist.php',
				data: {
					myData: dataString
				},
				type: 'POST'
			}).done(function (response) {
				if (response !== "OK") {
					alert("Le serveur a renvoyé l'erreur :\n", response);
				}
			}).fail(function (jqXHR, textStatus, error) {
				alert("Erreur lors de l'envoi du fichier hist :\n" + textStatus);
			});
		},

		"exporter": function () {
			// Sauvegarde locale du json permettant de donner un nom au fichier
			var a = document.createElement('a');
			a.href = 'data:attachment/json;charset=utf-8,' + JSON.stringify(gloutons);
			a.target = '_blank';
			a.download = 'gloutons.json';
			document.body.appendChild(a);
			a.click();
			document.body.removeChild(a);
		}
	};
})();
