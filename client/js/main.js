"use strict";

// TODO: tableau famille / commandes / sommes versées

// FIXME: Mieux gérer l'historique (actuellement augmente à l'infini)
// TODO: Proposer un archivage de l'historique
// TODO: Envoyer le fichier de fournée par mail/téléchargement lors de la validation (paramétrage du mail dans les options)
// TODO: Proposer un chargement de fichier de fournée

var fournil = (function () {

	$(document).ready(function () {

		// Message de confirmation lorsqu'on quitte la page
		$(window).bind('beforeunload', function () {
			return "Avez-vous pensé à enregistrer ?";
		});

		// Bouton de scroll to top
		$('#btn_Top').css({
			'display': 'none'
		});
		var offset = 800;
		var duration = 600;
		$(window).scroll(function () {
			if ($(this).scrollTop() > offset) {
				$('#btn_Top').fadeIn(duration);
			} else {
				$('#btn_Top').fadeOut(duration);
			}
		});

		$('#btn_Top').click(function (event) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: 0
			}, duration);
			return false;
		})

		f_utils.charger();

		// Clic sur le bouton envoyer
		$(document).on("click", "#btn-send-json", function (event) {
			f_utils.envoyer();
		});

		// Clic sur le bouton "Export"
		$(document).on("click", "#btn-export", function (event) {
			f_utils.exporter();
		});

	}); // => fin $(document).ready
})();
