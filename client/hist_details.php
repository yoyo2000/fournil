<?
try {
    $date = $_GET['date'];
    $fichier = "hist_f/" . $date . ".json";
    $json_output = file_get_contents($fichier);
    $ff = json_decode($json_output);
    $f = json_encode($ff, JSON_UNESCAPED_UNICODE);

    if ($ff->version == 2) {
        $p = json_encode($ff->pains, JSON_UNESCAPED_UNICODE);
    } else {
        $p = json_encode(json_decode('{"p500": { "nom": "Pain 500g", "poids": 0.5, "prix": 2.8 }, "p1000": { "nom": "Pain 1kg", "poids": 1, "prix": 5 }, "p1500": { "nom": "Pain 1,5kg", "poids": 1.5, "prix": 7.4 }, "p2000": { "nom": "Pain 2kg", "poids": 2, "prix": 9.8 }}'));
    }

    if (($f == "null") || ($p == "null")) {
        exit('Impossible de charger les données.');
    }
} catch (Exception $e) {
    exit('Erreur: ' . $e->getMessage());
}
?>
<!doctype html>
<html>

<head>
    <title>Fournil Historique Détails</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="favicon.png">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/open-iconic-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/datatables.min.css" rel="stylesheet" />
</head>

<body>
    <div id="fournee" class="content">
        <nav class="navbar sticky-top navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <img src="favicon.png" width="50" height="30" class="d-inline-block align-top" alt="">
                <span class="h3 ml-2">Fournil</span>
            </a>
            <a href="<? echo $fichier ?>" download class="btn btn-sm btn-outline-warning" role="button" title="Télécharger le fichier de cette fournée">
                <span id="down-json-icon' class='oi oi-cloud-download" aria-hidden="true"></span>
                <span id='down-json-label' class='ml-1'>Télécharger</span>
            </a>
            <span class="navbar-text">
                <span id="send-json-icon" class="oi oi-header" aria-hidden="true"></span>
                <span id="send-json-label" class="ml-1">Historique</span>
            </span>
        </nav>

        <div id="accordion" class="accordion card text-center mt-2">
            <div class="card-header collapsed h4 yo-pointer" data-toggle="collapse" data-target="#collapseOne" href="#collapseOne">
                <div class="row">
                    <div class="col">
                        <img src="favicon.png" width="50" height="30" class="d-none d-print-inline-block align-top" alt="">
                    </div>
                    <div class="col-8">
                        <a class="card-title">Fournée du
                            <u><mark id="date_nouvelle_fournee">{{formatJolieDate(fourneeEnCours.date)}}</mark></u>
                        </a>
                    </div>
                    <div class="col">
                        <img src="favicon.png" width="50" height="30" class="d-none d-print-inline-block align-top" alt="">
                    </div>
                </div>
            </div>
            <div id="collapseOne" class="card-block collapse show d-print-none" data-parent="#accordion">
                <div class="card-body">
                    <div class="row justify-content-md-center mb-3" id="recap">
                        <div class="col col-lg-2"></div>
                        <div v-if="fourneeEnCours.version==2" class="col-md-8">
                            <div>
                                <table class="table table-striped table-sm table-bordered shadow">
                                    <thead>
                                        <tr>
                                            <template v-for="pain in pains">
                                                <th class="text-center text-danger align-middle">
                                                    {{ pain.nom}}
                                                </th>
                                            </template>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <template v-for="pain in pains">
                                                <th class="text-center text-info align-middle">
                                                    {{pain.prix}}&nbsp;€
                                                </th>
                                            </template>
                                        </tr>
                                        <tr>
                                            <template v-for="(pain, p_id) in pains">
                                                <td class="text-center table-danger">{{ fourneeEnCours.sommes[p_id].nombre }}</td>
                                            </template>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col col-lg-2"></div>
                            <div class="col col-lg-2"></div>
                            <div>
                                <table class="table table-striped table-sm table-bordered shadow">
                                    <thead>
                                        <tr>
                                            <th class="text-center text-warning align-middle">{{ typePains.p }}</th>
                                            <th class="text-center text-warning align-middle">{{ typePains.g }}</th>
                                            <th class="text-center text-warning align-middle">{{ typePains.s }}</th>
                                            <th class="text-center table-warning">Poids Total Pains</th>
                                            <th class="text-center text-warning align-middle">{{ typePains.e }}</th>
                                            <th class="text-center text-warning align-middle">{{ typePains.b }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center table-warning">
                                                {{ arrondiCalcul(fourneeEnCours.sommes.total.poids.p) }}&nbsp;kg
                                            </td>
                                            <td class="text-center table-warning">
                                                {{ arrondiCalcul(fourneeEnCours.sommes.total.poids.g) }}&nbsp;kg
                                            </td>
                                            <td class="text-center table-warning">
                                                {{ arrondiCalcul(fourneeEnCours.sommes.total.poids.s) }}&nbsp;kg
                                            </td>
                                            <th class="text-center table-warning">
                                                {{ arrondiCalcul(fourneeEnCours.sommes.total.poids.p+fourneeEnCours.sommes.total.poids.g+fourneeEnCours.sommes.total.poids.s) }}&nbsp;kg
                                            </th>
                                            <td class="text-center table-warning">
                                                {{ arrondiCalcul(fourneeEnCours.sommes.total.poids.e) }}&nbsp;kg
                                            </td>
                                            <td class="text-center table-warning">
                                                {{ arrondiCalcul(fourneeEnCours.sommes.total.poids.b) }}&nbsp;kg
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div v-else class="col-md-6">
                            <table class="table table-striped shadow">
                                <thead>
                                    <tr>
                                        <template v-for="pain in pains">
                                            <th class="text-center text-danger">
                                                {{ pain.nom.split(" ")[0] }}<br />{{ pain.nom.split(" ")[1] }}
                                            </th>
                                        </template>
                                        <th class="table-warning">Poids<br />Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <template v-for="(pain, p_id) in pains">
                                            <td class="text-center table-danger">
                                                {{ fourneeEnCours.sommes[p_id].nombre }}
                                            </td>
                                        </template>
                                        <th class="table-warning">{{ fourneeEnCours.sommes.total.poids + ' kg' }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col col-lg-2"></div>
                    </div>
                    <div id="panier">
                        <table id="TableauPanier" class="table table-bordered table-sm yo-align">
                            <tbody>
                                <tr>
                                    <th v-bind:class="(fourneeEnCours.panier['cheque']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/cheque.png" alt="chèques" title="chèques"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['500']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/500.png" alt="500" title="500"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['200']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/200.png" alt="200" title="200"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['100']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/100.png" alt="100" title="100"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['50']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/50.png" alt="50" title="50"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['20']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/20.png" alt="20" title="20"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['10']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/10.png" alt="10" title="10"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['5']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rect" src="images/euros/5.png" alt="5" title="5"></th>
                                </tr>
                                <tr>
                                    <td>{{fourneeEnCours.panier.cheque}}</td>
                                    <td>{{fourneeEnCours.panier['500']}}</td>
                                    <td>{{fourneeEnCours.panier['200']}}</td>
                                    <td>{{fourneeEnCours.panier['100']}}</td>
                                    <td>{{fourneeEnCours.panier['50']}}</td>
                                    <td>{{fourneeEnCours.panier['20']}}</td>
                                    <td>{{fourneeEnCours.panier['10']}}</td>
                                    <td>{{fourneeEnCours.panier['5']}}</td>
                                </tr>
                                <tr>
                                    <th v-bind:class="(fourneeEnCours.panier['2']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/2.png" alt="2" title="2"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['1']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/1.png" alt="1" title="1"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['05']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/05.png" alt="05" title="05"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['02']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/02.png" alt="02" title="02"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['01']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/01.png" alt="01" title="01"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['005']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/005.png" alt="005" title="005"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['002']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/002.png" alt="002" title="002"></th>
                                    <th v-bind:class="(fourneeEnCours.panier['001']==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond" src="images/euros/001.png" alt="001" title="001"></th>
                                </tr>
                                <tr>
                                    <td>{{fourneeEnCours.panier['2']}}</td>
                                    <td>{{fourneeEnCours.panier['1']}}</td>
                                    <td>{{fourneeEnCours.panier['05']}}</td>
                                    <td>{{fourneeEnCours.panier['02']}}</td>
                                    <td>{{fourneeEnCours.panier['01']}}</td>
                                    <td>{{fourneeEnCours.panier['005']}}</td>
                                    <td>{{fourneeEnCours.panier['002']}}</td>
                                    <td>{{fourneeEnCours.panier['001']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row justify-content-center">
                            <table class="table table-borderless yo-align col-4" v-bind:class="(fourneeEnCours.sommes.total.versement!=fourneeEnCours.sommes.total.panier || fourneeEnCours.sommes.total.versement==0) ? 'shadow' : 'shining'">
                                <tbody>
                                    <tr>
                                        <td class="col-2" v-bind:class="(fourneeEnCours.sommes.total.panier==0) ? 'bg-light' : 'table-warning'"><img class="yo-svg-rond2" src="images/panier.png" alt="panier" title="panier"></td>
                                        <td class="col-2" v-bind:class="(fourneeEnCours.sommes.total.versement!=fourneeEnCours.sommes.total.panier) ? 'table-danger' : 'table-warning'"><img class="yo-svg-rond2" v-bind:src="(fourneeEnCours.sommes.total.versement!=fourneeEnCours.sommes.total.panier) ? 'images/don-bas.png' : 'images/don.png'" alt="don" title="don"></td>
                                    </tr>
                                    <tr>
                                        <td class="h3" v-bind:class="(fourneeEnCours.sommes.total.panier==0) ? 'bg-light' : 'table-warning'">{{ affichePrix(fourneeEnCours.sommes.total.panier) }}</td>
                                        <td class="h3" v-bind:class="(fourneeEnCours.sommes.total.versement!=fourneeEnCours.sommes.total.panier) ? 'table-danger' : 'table-warning'">{{ affichePrix(fourneeEnCours.sommes.total.versement) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table id="GloutonsFourneeEnCours" class="table table-hover table-bordered yo-taille">
            <thead class="thead-light">
                <tr>
                    <th scope="col"><span class="oi oi-tag" aria-hidden="true"></span></th>
                    <th scope="col"><span class="oi oi-person" aria-hidden="true"></span><span class="ml-1">Nom</span></th>
                    <template v-for="(pain, p_id) in pains">
                        <th class="text-center" scope="col">
                            <span class="text-danger">{{ pain.nom }}<br />{{ fourneeEnCours.sommes[p_id].nombre }}</span>
                        </th>
                    </template>
                    <th class="text-center" scope="col"><span class="oi oi-check" aria-hidden="true"></span><span class="ml-1">Somme<br />due</span></th>
                    <th class="text-center" scope="col"><span class="oi oi-thumb-up" aria-hidden="true"></span><span class="ml-1">Somme<br />versée</span></th>
                    <th class="text-center" scope="col"><span class="oi oi-euro" aria-hidden="true"></span><span class="ml-1">Solde<br />actuel</span></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(glou, index) in fourneeEnCours.gloutons" :id="'tr-fourneeencours-'+index">
                    <th scope="row">{{ index + 1 }}</th>
                    <td>
                        <strong>{{ glou.nom }}</strong>
                    </td>
                    <template v-for="(pain, p_id) in pains">
                        <td class="table-danger text-center align-middle">{{ glou.articles[p_id] }}</td>
                    </template>
                    <td class="text-center">{{affichePrix(glou.due)}}</td>
                    <td class="table-warning text-center">{{affichePrix(glou.verse)}}</td>
                    <td class="text-center"><strong>{{ affichePrix(glou.solde) }}</strong></td>
                </tr>
            </tbody>
        </table>
        <div class="alert alert-danger text-center" v-show="fourneeEnCours.gloutons.length == 0">
            <span class="oi oi-double-quote-serif-left" aria-hidden="true"></span>
            <strong class="yo-respire">Personne pour cette fournée !</strong>
            <span class="oi oi-double-quote-serif-right" aria-hidden="true"></span>
        </div>


    </div>
</body>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/vue.js"></script>
<? echo "<script>var f = " . $f . "\nvar p = " . $p . ";</script>\n"; ?>
<script>
    "use strict";

    String.prototype.capitalize = function() {
        // Met en majuscule la 1ère lettre
        return this.charAt(0).toUpperCase() + this.substr(1);
    }

    var vm_fournee = new Vue({
        el: '#fournee',
        data: {
            pains: p,
            fourneeEnCours: f,
            typePains: {
                "p": "Froment",
                "g": "Engrain",
                "s": "Seigle",
                "e": "Épices",
                "b": "Biscuits"
            }
        },
        created() {
            // Changement du title
            document.title += " " + this.fourneeEnCours.date;
        },
        methods: {
            affichePrix: function(prix) {
                // Retourne un prix formaté à partir d'un float
                // 125665.789 returns "125 665,79 €"
                // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NumberFormat
                return new Intl.NumberFormat('fr-FR', {
                    style: 'currency',
                    currency: 'EUR'
                }).format(prix);
            },
            formatJolieDate: function(jour) {
                // Convertit un jour "2018-12-16" en jolie date formatée "Dimanche 16 décembre 2018"
                try {
                    var listeJours_d = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
                    var mm = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
                    var date = new Date(jour);
                    return listeJours_d[date.getDay()].capitalize() + " " + date.getDate() + " " + mm[date.getMonth()] + " " + date.getFullYear();
                } catch (error) {
                    console.log(error);
                    return jour;
                }
            },
            arrondiCalcul: function(val) {
                // Retourne une valeur arrondie à 2 chiffres à partir d'un float
                if (!val) return 0;
                if (typeof(val) == "string") val = parseFloat(val);
                let x = parseFloat(val.toFixed(2));
                return isNaN(x) ? 0 : x;
            },
            capitalize: String.prototype.capitalize
        }

    });
</script>

</html>