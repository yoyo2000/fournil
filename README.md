## Description

Gestion d'un fournil type AMAP où les clients (gloutons) se servent et payent seuls.

* Chargement des gloutons depuis le serveur (fichier json)
* Export sur le serveur du fichier des gloutons (bouton Sauvegarder)
* Affichage en 2 onglets : gloutons actifs, gloutons suspendus (non pris en compte pour les fournées)
* Affichage différencié des commandes récurrentes, ponctuelles à venir, ponctuelles dépassées
* Filtrage de l'affichage des gloutons / gloutons suspendus avec une zone de recherche (se base sur le nom)
* Suspension / Rétablissement / Suppression de gloutons
* Ajout/Édition des informations des gloutons (nom, mail, téléphone, solde avec possibilité d'ajouter des sommes, annotations pour la fournée en cours)
* Ajout/Édition des commandes des gloutons :
    * Commandes récurrentes
    * Commandes ponctuelles pour un jour de la semaine suivante
    * Commandes ponctuelles sur une date précise
    * Reprise des articles de la commande récurrente correspondante lors de l'ajout d'une commande ponctuelle
    * Vacances : saisie de commandes vides entre 2 dates (se base sur les commandes récurrentes)
* Onglet Nouvelle fournée / Fournée en cours :
    * Création d'une nouvelle fournée basée sur les jours de fournée cochés dans l'onglet paramètres et sur les commandes des gloutons actifs
    * Reprise d'une fournée en cours (précédement sauvegardée)
    * Tableau récapitulatif des articles commandés avec poids total
    * Tableau des commandes par glouton à imprimer et permettant de saisir les sommes versées par les gloutons
    * La fournée en cours se rafraichit automatiquement si les commandes des gloutons évoluent
    * Tableau de saisie du panier reçu pour la fournée selon les billets/pièces/chèques (clic gauche => +1 / clic droit => -1)
    * Pour la saisie des chèques, possibilité d'entrer directement le total ou bien une somme à rajouter en commençant par "+"
    * Possibilité d'exclure un glouton de la fournée en cours (génère une commande ponctuelle vide)
    * Possibilité d'imprimer sélectivement les gloutons de la fournée en cours (clic sur le numéro)
    * Bouton imprimer (penser à activer l'impression de la couleur d'arrière-plan dans les options d'impression)
    * Bouton validation fournée :
        * Vérifie que les versements des gloutons ont bien été saisis
        * Vérifie que le panier et la somme des versements correspondent
        * Calcule le nouveau solde des gloutons
        * Supprime les commandes ponctuelles liées à cette fournée
        * Bascule les billets/pièces/chèques du panier vers la tirelire
        * Archive les données de la fournée
        * Remet à zéro la fournée en cours
* Onglet Prévisions :
    * Affiche le poids prévisionnel des prochaines fournées à partir des commandes récurrentes et ponctuelles des gloutons actifs
* Onglet Paramètres :
    * Affichage des articles (prix et poids des pains)
    * Possibilité d'éditer directement le prix des pains
    * Enregistrement des jours de fournée (donnée conservée dans le navigateur)
    * Possibilité de sauvegarde automatique lors d'un clic sur les boutons Imprimer et Valider de la fournée en cours
    * Possibilité de télécharger le fichier des gloutons (format json)
* Onglet Tirelire :
    * Après validation, les billets/pièces/chèques des fournées vont remplir la tirelire
    * Possibilité de modifier chaque valeur (clic gauche => +1 / clic droit => -1)
    * Bouton de remise à zéro
    * Affichage de la somme totale des soldes des gloutons
* Page d'historique :
    * Affichage d'un tableau reprenant les fournées passées avec leur date, panier total et poids total
    * Possibilité de voir les détails de chaque fournée de l'historique
    * Possibilité de télécharger le fichier de fournée (format json)

Testé uniquement sous Firefox.

Pour tester, lancer `client/index.html` dans un navigateur.

## License

WTFPL : http://www.wtfpl.net/
