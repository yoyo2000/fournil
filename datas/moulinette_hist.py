#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Parcours l'ancien historique global pour le scinder en autant de fichier que de fournées et un fichier d'historique allégé
"""
import json

json_file = "hist.json"

with open(json_file, "r", encoding="utf-8") as fich:
    hist = json.load(fich)

new_hist = {}
for d, f in hist["historique"].items():
    # print(f'Date : {d}, Panier : {f["sommes"]["total"]["panier"]} €, Poids : {f["sommes"]["total"]["poids"]} kg')
    with open(f"new_hist_f/{d}.json", "w", encoding="utf-8") as fich:
        json.dump(f, fich, ensure_ascii=False, indent=2)
    
#     new_hist[d] = {
#         "poids": f["sommes"]["total"]["poids"],
#         "panier": f["sommes"]["total"]["panier"],
#     }

# print(new_hist)
# with open("new_hist_f/new_hist.json", "w", encoding="utf-8") as fich:
#     json.dump(new_hist, fich, ensure_ascii=False, indent=2)
