#!/usr/bin/python3
# coding: utf-8
"""Télécharge le fichier de donnée du fournil (datas.json), le compresse en zip,
	le renomme avec pour suffixe la date du jour, et supprime les anciens fichiers
	de sauvegarde (rotation)
"""

import datetime as dt

from sys import exit
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED
from urllib.request import urlretrieve

SAV_PATH = Path(__file__).parent / "private" / "sav"
MAX_ZIP_FILE = 10	# nombre maxi de fichier zip avant rotation

def now():
    """Retourne la date et l'heure courante au format 2018-12-16T10-30-23"""
    return dt.datetime.now().isoformat().split(".")[0].replace(":", "-")

def purge(max):
    """Supprime le plus ancien fichier zip si leur nombre est supérieur à max"""
    lzip = sorted(list(SAV_PATH.glob("*.zip")), key=str)
    if len(lzip) > max:
        lzip[0].unlink()

def main():
	try:
		from private.ftpconfig import url, login, password
	except Exception as err:
		print('Erreur : Impossible de charger le fichier de configuration private/ftpconfig.py')
		exit(1)

	if not(Path.is_dir(SAV_PATH)):
		Path.mkdir(SAV_PATH)

	datasource = SAV_PATH / "datas.json"
	urlretrieve(f'ftp://{login}:{password}@{url}', datasource)

	datazip = SAV_PATH / f"f-{now()}.zip"
	with ZipFile(datazip, 'w') as save:
		save.write(datasource, "datas.json", compress_type=ZIP_DEFLATED)

	purge(MAX_ZIP_FILE)


if __name__ == '__main__':
	main()