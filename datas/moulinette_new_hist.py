#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Parcours un dossier contenant des fichiers de fournée et génère le fichier d'historique allégé
"""
import json
from pathlib import Path

NEW_HIST_FILE = "new_hist_f/new_hist.json"
liste_fournees = list(
    Path("new_hist_f").glob("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].json")
)

new_hist = {}
for fournee in liste_fournees:
    with open(fournee, "r", encoding="utf-8") as fich:
        f = json.load(fich)

    print(
        f'Date : {f["date"]}\t Panier : {f["sommes"]["total"]["panier"]} € \t Poids : {f["sommes"]["total"]["poids"]} kg'
    )

    new_hist[f["date"]] = {
        "poids": f["sommes"]["total"]["poids"],
        "panier": f["sommes"]["total"]["panier"],
    }

print(f"Total : {len(new_hist)} fournées")

with open(NEW_HIST_FILE, "w", encoding="utf-8") as fich:
    json.dump(new_hist, fich, ensure_ascii=False, indent=2)
