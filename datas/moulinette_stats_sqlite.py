#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Parcours un dossier contenant des fichiers de fournée et génère le fichier de stats
"""
import json
import sqlite3
from sys import exit
from pathlib import Path

SAV_PATH = Path("/home/yo/Sauv/fournil")
conn = sqlite3.connect(SAV_PATH / "stats.sqlite3")

drop = "DROP TABLE IF EXISTS stats"
req_count = "SELECT COUNT ( DISTINCT datef ) FROM stats"
req_init = """CREATE TABLE IF NOT EXISTS [stats] (
[_id] INTEGER PRIMARY KEY,
[datef] TEXT NULL,
[glouton] TEXT NULL,
[p500] INT NULL,
[p1000] INT NULL,
[p1500] INT NULL,
[p2000] INT NULL,
[g650] INT NULL,
[s500] INT NULL,
[e640] INT NULL,
[e900] INT NULL,
[b75] INT NULL,
[b150] INT NULL,
[verse] NUMERIC NULL
);
"""

liste_fournees = list(
    Path(SAV_PATH / "hist_f").glob("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].json")
)
liste_fournees.sort()

cur = conn.cursor()

nb_cur = cur.execute(req_count)
nb_f = nb_cur.fetchone()[0]
if nb_f == len(liste_fournees):
    print("Pas de nouvelle fournée.")
    exit(0)

cur.execute(drop)
cur.execute(req_init)
conn.commit()

req_insert_init = "INSERT INTO stats(`datef`,`glouton`,`p500`,`p1000`,`p1500`,`p2000`,`g650`,`s500`,`e640`,`e900`,`b75`,`b150`, `verse`) VALUES "
for fournee in liste_fournees:
    with open(fournee, "r", encoding="utf-8") as fich:
        f = json.load(fich)

    for g in f["gloutons"]:
        req_insert_values = f'("{f["date"]}","{g["nom"]}",{g["articles"].get("p500",0)},{g["articles"].get("p1000",0)},{g["articles"].get("p1500",0)},{g["articles"].get("p2000",0)},{g["articles"].get("g650",0)},{g["articles"].get("s500",0)},{g["articles"].get("e640",0)},{g["articles"].get("e900",0)},{g["articles"].get("b75",0)},{g["articles"].get("b150",0)},{g["verse"]})'
        # print(req_insert_values)
        cur.execute(req_insert_init + req_insert_values)

    req_insert_values = f'("{f["date"]}","[TOUS]",{f["sommes"]["p500"]["nombre"]},{f["sommes"]["p1000"]["nombre"]},{f["sommes"]["p1500"]["nombre"]},{f["sommes"]["p2000"]["nombre"]},{f["sommes"].get("g650",{}).get("nombre",0)},{f["sommes"].get("s500",{}).get("nombre",0)},{f["sommes"].get("e640",{}).get("nombre",0)},{f["sommes"].get("e900",{}).get("nombre",0)},{f["sommes"].get("b75",{}).get("nombre",0)},{f["sommes"].get("b150",{}).get("nombre",0)},{f["sommes"]["total"]["versement"]})'
    # print(req_insert_values)

    cur.execute(req_insert_init + req_insert_values)
    conn.commit()

print(f"Total : {len(liste_fournees)} fournées")

conn.close()
