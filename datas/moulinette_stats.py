#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Parcours un dossier contenant des fichiers de fournée et génère le fichier de stats
"""
import json
from pathlib import Path

CSV_FILE = "stats.csv"
liste_fournees = list(
    Path("hist_f").glob("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].json")
)

stats = "date;nb p500;coût p500;nb p1000;coût p1000;nb p1500;coût p1500;nb p2000;coût p2000\n"
for fournee in liste_fournees:
    with open(fournee, "r", encoding="utf-8") as fich:
        f = json.load(fich)

    s = f'{f["date"]};{f["sommes"]["p500"]["nombre"]};{round(f["sommes"]["p500"]["prix"],1)};{f["sommes"]["p1000"]["nombre"]};{round(f["sommes"]["p1000"]["prix"],1)};{f["sommes"]["p1500"]["nombre"]};{round(f["sommes"]["p1500"]["prix"],1)};{f["sommes"]["p2000"]["nombre"]};{round(f["sommes"]["p2000"]["prix"],1)}\n'
    print(s)

    stats += s

print(f"Total : {len(stats)} fournées")

with open(CSV_FILE, "w", encoding="utf-8") as fich:
    fich.write(stats)
