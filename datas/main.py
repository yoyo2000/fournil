#!/usr/bin/python3
# coding: utf-8

import csv
import io, json
from locale import *

if __name__ == "__main__":

    setlocale(LC_NUMERIC, "")

    jours = ["mardi", "vendredi"]

    pains = {}
    with open("pains.csv", newline="", encoding="utf-8") as f:
        reader = csv.reader(f, delimiter=";", quoting=csv.QUOTE_NONE)
        for row in reader:
            # code, dénomination, poids, prix
            pains[row[0]] = {
                "nom": row[1],
                "poids": float(row[2]),
                "prix": float(row[3]),
            }
    #print(pains)

    #print("==============")

    gloutons = {}   # {'Famille Toto': {'nom': 'Famille Toto', 'mail': 'toto@free.fr', 'tel': '01.02.03.04.05', 'solde': -15.2, 'commande': {}}
    with open("private/g-infos.csv", newline="", encoding="utf-8") as f:
        reader = csv.reader(f, delimiter="\t", quoting=csv.QUOTE_NONE)
        for i, row in enumerate(reader):
            # tel, mail, nom, solde
            nom = row[2]
            gloutons[nom] = {
                "nom": row[2],
                "mail": row[1],
                "tel": row[0],
                "solde": atof(row[3]),
                "commande": {}
            }

    for j in jours:
        with open("private/g-{}.csv".format(j), newline="", encoding="utf-8") as f:
            reader = csv.reader(f, delimiter="\t", quoting=csv.QUOTE_NONE)
            for i, row in enumerate(reader):
                # nom, p500, p1000, p1500, p2000
                com_j = {}
                p_ids = ["p500", "p1000", "p1500", "p2000"]
                for c in range(4):
                    #print("{0} : {1}".format(c + 1, row[c + 1]))
                    if row[c + 1] and row[c + 1] != '0':
                        com_j.update({p_ids[c]: int(row[c + 1])})
                nom = row[0]
                if nom not in gloutons:
                    gloutons[nom] = {"nom": nom, "mail": "", "tel": "", "solde": 0, "commande": {}}
                if com_j:
                    gloutons[nom]["commande"][j] = com_j


    #print(gloutons)

    listG = [g for g in gloutons.values()]
    for g in listG:
        # Mise en attente si aucune commande
        if g["commande"] == {}:
            g["attente"] = "true"
        
        # Remplace des caractères de séparation par " / " sur le tel
        g["tel"] = "".join(g["tel"].split()).replace("-"," ").replace("//", " ").replace("/", " ").replace(";"," ").replace(",", " ").replace(" ", " / ")
        
        # Remplace des caractères de séparation par une espace sur le mail
        g["mail"] = "".join(g["mail"].split()).replace("/", " ").replace(";"," ").replace(",", " ")

    #print(listG)

    dico = {"gloutons": listG, "pains": pains}
    #print(dico)
    with io.open("private/datas.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(dico, ensure_ascii=False))
