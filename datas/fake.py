#!/usr/bin/python3
# coding: utf-8

import csv
import io, json
from random import randint, choice, choices # https://docs.python.org/fr/3/library/random.html#random.choices
from faker import Faker	# https://github.com/joke2k/faker

if __name__ == "__main__":

	jours_f = ["mardi", "vendredi"]	# jours privilégiés pour les fournées
	all_j = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

	pains = {}
	with open('pains.csv', newline='', encoding='utf-8') as f:
		reader = csv.reader(f, delimiter=';', quoting=csv.QUOTE_NONE)
		for row in reader:
			# code, dénomination, poids, prix
			pains.update({row[0]: {"nom": row[1], "poids": float(row[2]), "prix": float(row[3])}})

	gloutons = []	# nom, mail, tel, solde, commande = {jour: {pain1.id: nb, ...}, jour2: {...}}
	f = Faker("fr_FR")	# localisation fr du faker

	# 150 personnes avec des commandes seulement le mardi et vendredi => jours_f
	for i in range(15):
		nom = f.name()
		mail = f.email()
		tel = f.phone_number()
		solde = randint(-20, 20)	# solde entre -20.0 et 20.0
		commande = {}
		# choices(une_liste, k=nombre_de_choix_à_faire, weights=liste_de_poids_de_chaque_choix) retourne une liste
		# ici on veut le plus souvent (70 %) les 2 jours de fournées (mardi et vendredi) et plutot un seul (80 %) pain de 1kg (60 %) en quantité 1 (70 %)
		for j in choices(jours_f, k=choices([1,2], weights=[0.3, 0.7],k=1)[0]):
			for p in choices(list(pains.keys()), weights=[0.2, 0.6, 0.2, 0.2], k=choices([1,2,3], weights=[0.8, 0.1, 0.1],k=1)[0]):
				if not j in commande:
					commande[j] = {p : int(choices([1,2,3,4], weights=[0.7, 0.1, 0.1, 0.1],k=1)[0])}
				else:
					commande[j].update({p : int(choices([1,2,3,4], weights=[0.7, 0.1, 0.1, 0.1],k=1)[0])})
		# ici commande est retriée par jour de la semaine => appel à all_j
		gloutons.append({"nom": nom, "mail": mail, "tel": tel, "solde": float(solde), "commande": {j:commande[j] for j in all_j if j in commande}})

	# 50 personnes avec des commandes aléatoires
	for i in range(5):
		nom = f.name()
		mail = f.email()
		tel = f.phone_number()
		solde = randint(-20, 20)
		commande = {}
		# ici on veut le plus souvent (80 %) un seul jour aléatoire et plutot un seul (80 %) pain de 1kg (60 %) en quantité 1 (70 %)
		for j in range(choices([1,2], weights=[0.8, 0.2],k=1)[0]):
			j = f.day_of_week().lower()
			for p in choices(list(pains.keys()), weights=[0.2, 0.6, 0.2, 0.2], k=choices([1,2,3], weights=[0.8, 0.1, 0.1],k=1)[0]):
				if not j in commande:
					commande[j] = {p : int(choices([1,2,3,4], weights=[0.7, 0.1, 0.1, 0.1],k=1)[0])}
				else:
					commande[j].update({p : int(choices([1,2,3,4], weights=[0.7, 0.1, 0.1, 0.1],k=1)[0])})
		# ici commande est retriée par jour de la semaine => appel à all_j
		gloutons.append({"nom": nom, "mail": mail, "tel": tel, "solde": float(solde), "commande": {j:commande[j] for j in all_j if j in commande}})

	dico = {"gloutons": gloutons, "pains": pains}

	with io.open('../client/datas_new.json', 'w', encoding='utf-8') as f:
		f.write(json.dumps(dico, ensure_ascii=False))
